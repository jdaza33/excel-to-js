import Vue from 'vue'
import VueRouter from 'vue-router'
import Cookies from 'js-cookie'
import Selector from './selector'
import Login from '../views/Login'
import Main from '../views/Main'
import NotFound from '../views/NotFound'
import Wait from '../views/Wait'
import Users from '../views/Users'

//Services
import { checkToken } from '../services/user'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Init',
    redirect: '/login',
  },
  {
    path: '/login',
    name: 'Login',
    component: Login,
    meta: { checkAuth: true },
  },
  {
    path: '/main',
    name: 'Main',
    component: Main,
    meta: { requiredAuth: true },
    redirect: '/wait',
    children: [
      {
        path: '/re/:name',
        name: 'Re',
        component: Selector,
        props: (route) => ({ name: route.params.name }),
        meta: { requiredAuth: true },
      },
      {
        path: '/wait',
        name: 'Wait',
        component: Wait,
        meta: { requiredAuth: true },
      },
      {
        path: '/users',
        name: 'Users',
        component: Users,
        meta: { requiredAuth: true },
      },
    ],
  },
  {
    path: '*',
    name: 'NotFound',
    component: NotFound,
  },
]

const router = new VueRouter({
  mode: 'history',
  routes,
})

router.beforeEach((to, from, next) => {
  let token = Cookies.get('token')
  let userId = Cookies.get('userId')

  // console.log(to)
  // console.log(token, userId)

  if (to.meta.requiredAuth) {
    if (token && userId) {
      checkToken(token, userId).then(({ success, message, isAccess }) => {
        if (success == 1) {
          if (isAccess) next()
        } else router.push('/login')
      })
    } else router.push('/login')
  } else if (to.meta.checkAuth) {
    if (token && userId) {
      checkToken(token, userId).then(({ success, message, isAccess }) => {
        if (success == 1) {
          if (isAccess) router.push('/main')
        } else next()
      })
    } else next()
  } else next()
})

export default router
