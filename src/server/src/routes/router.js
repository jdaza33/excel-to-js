/**
 * Rutas
 */

//Modules
const express = require('express')

//Constantes
const router = express.Router()

//Controllers
const userCtrl = require('../controllers/user.controller')

/** Users */
router.post('/users/create', userCtrl.createUser)
router.post('/users/list', userCtrl.listUsers)
router.post('/users/login', userCtrl.loginUser)
router.post('/users/check-token', userCtrl.checkTokenUser)
router
  .get('/users/:id', userCtrl.getUser)
  .put('/users/:id', userCtrl.editUser)
  .delete('/users/:id', userCtrl.delUser)

router.use('*', (req, res) => {
  return res.status(404).send({
    success: 0,
    data: null,
    error: {
      status: 404,
      type: 'Resource not found',
      reason: 'Application does not support the requested path.',
    },
  })
})

module.exports = router
