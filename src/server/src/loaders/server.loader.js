/**
 * @description Server
 */

// Modules
const express = require('express')
const cors = require('cors')
const helmet = require('helmet')
const paginate = require('express-paginate')
const compression = require('compression')
const history = require('connect-history-api-fallback')
const path = require('path')

//Routes
const Router = require('../routes/router')

//Middlewares
const { errorHandler } = require('../middlewares/error.middleware')

//Constantes
const publicPath = path.resolve(__dirname, '../../../../', './dist')
const staticConf = { maxAge: '1y', etag: false }

const startExpress = () => {
  return new Promise(async (resolve, reject) => {
    try {
      //APP
      const app = express()

      //Middlewares
      app.use(cors())
      // app.use(helmet()) /**NO FUNCIONA PARA LEVANTAR UNA INSTANCIA DE VUEJS */
      app.use(express.json())
      app.use(paginate.middleware(10, 50))
      app.use(compression())

      //Views
      console.log(publicPath)
      // app.set('view engine', 'html')
      app.use(express.static(publicPath, staticConf))
      app.use(history())
      app.use(express.static(publicPath, staticConf))
      app.get('/', function(req, res) {
        res.render(path.join(__dirname, 'index.html'))
      })

      //Routes
      app.use('/api', Router)

      //Error
      app.use(errorHandler)

      return resolve(app)
    } catch (error) {
      return reject(error)
    }
  })
}

module.exports = { startExpress }
