/**
 * @description Conexion a la base de datos de MongoDB
 */

const mongoose = require('mongoose')

const startMongoose = () => {
  return new Promise(async (resolve, reject) => {
    try {
      // setDefaultOptions()

      const connection = await mongoose.connect(env.URL_DATABASE, {
        useCreateIndex: true,
        useNewUrlParser: true,
        useFindAndModify: false,
        useUnifiedTopology: true,
      })

      logger.info(`Base de datos conectada con éxito`)

      return resolve(connection)
    } catch (error) {
      return reject(error)
    }
  })
}

/**
 * @description Funcion para aplicar la
 * propiedad lean a las consultas de mongoose
 */
const setDefaultOptions = () => {
  const __setOptions = mongoose.Query.prototype.setOptions

  mongoose.Query.prototype.setOptions = (options, overwrite) => {
    __setOptions.apply(this, arguments)
    if (!this.mongooseOptions().lean) this.mongooseOptions().lean = true
    return this
  }
}

module.exports = { startMongoose }
