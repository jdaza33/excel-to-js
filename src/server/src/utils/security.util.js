/**
 * @description Seguridad de utilidad
 */

//Modules
const bcrypt = require('bcrypt')
const jwt = require('jwt-simple')
const moment = require('moment')

//Constantes
const saltRounds = 10

const createHash = (text) => {
  try {
    return bcrypt.hash(text, saltRounds)
  } catch (error) {
    return error
  }
}

const compareHash = (hash, text) => {
  try {
    return bcrypt.compare(text, hash)
  } catch (error) {
    return error
  }
}

const createTokenJwt = (userId) => {
  try {
    let payload = {
      id: userId,
      expMs: moment()
        .set('day', 7)
        .valueOf(),
    }
    return jwt.encode(payload, env.SECRET_JWT)
  } catch (error) {
    return error
  }
}

const checkTokenJwt = (token, userId) => {
  try {
    let { id, expMs } = jwt.decode(token, env.SECRET_JWT)
    let now = moment().valueOf()

    if (id != userId) return false
    if (expMs < now) return false

    return true
  } catch (error) {
    return error
  }
}

module.exports = {
  createHash,
  compareHash,
  createTokenJwt,
  checkTokenJwt,
}
