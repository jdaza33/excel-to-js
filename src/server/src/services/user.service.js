/**
 * @description Servicio de usuarios
 */

//Models
const User = require('../models/user')

//Utils
const {
  createHash,
  compareHash,
  createTokenJwt,
  checkTokenJwt,
} = require('../utils/security.util')

const create = (data) => {
  return new Promise(async (resolve, reject) => {
    try {
      data.password = await createHash(data.password || 123456)
      data.createdAt = Date.now()

      let user = await User.create(data)

      return resolve(user)
    } catch (error) {
      return reject(error)
    }
  })
}

const user = (id) => {
  return new Promise(async (resolve, reject) => {
    try {
      let user = await User.findOne({ _id: id })
      return resolve(user)
    } catch (error) {
      return reject(error)
    }
  })
}

const list = (filters = {}) => {
  return new Promise(async (resolve, reject) => {
    try {
      let users = await User.find(filters)

      return resolve(users)
    } catch (error) {
      return reject(error)
    }
  })
}

const edit = (id, changes) => {
  return new Promise(async (resolve, reject) => {
    try {
      /**
       * @todo
       * Falta cambiar de forma eficiente los roles
       */

      if (changes.password) changes.password = createHash(changes.password)

      let userUpdated = await User.findOneAndUpdate({ _id: id }, changes, {
        new: true,
      })

      return resolve(userUpdated)
    } catch (error) {
      return reject(error)
    }
  })
}

const del = (id) => {
  return new Promise(async (resolve, reject) => {
    try {
      let userDeleted = await User.findOneAndRemove({ _id: id }, { new: true })
      return resolve(userDeleted)
    } catch (error) {
      return reject(error)
    }
  })
}

const login = (username, password) => {
  return new Promise(async (resolve, reject) => {
    try {
      if (!username || !password)
        return reject('Username y contraseña son requeridos')

      let user = await User.findOne({ username })
      user = JSON.parse(JSON.stringify(user))

      if (!user) return reject('El usuario no existe')
      if (!user.active) return reject('El usuario no esta activo')

      if (!compareHash(password, user.password))
        return reject('La contraseña invalida')

      let token = createTokenJwt(user._id.toString())
      user.token = token
      delete user.password
      return resolve(user)
    } catch (error) {
      return reject(error)
    }
  })
}

const checkToken = (token, userId) => {
  return new Promise(async (resolve, reject) => {
    try {
      return resolve(checkTokenJwt(token, userId))
    } catch (error) {
      return reject(error)
    }
  })
}

module.exports = { create, user, list, edit, del, login, checkToken }
