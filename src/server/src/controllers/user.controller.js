/**
 * @description Controlador de usuarios
 */

//Services
const {
  create,
  user,
  list,
  edit,
  del,
  login,
  checkToken,
} = require('../services/user.service')

const createUser = async (req, res, next) => {
  let user = await create(req.body)

  res.status(200).json({
    success: 1,
    user,
    message: 'Usuario creado con éxito',
  })
  try {
  } catch (error) {
    next(error)
  }
}

const getUser = async (req, res, next) => {
  try {
    let { id } = req.params
    let _user = await user(id)
    res.status(200).json({
      success: 1,
      user: _user,
      message: 'Usuario obtenido con éxito',
    })
  } catch (error) {
    next(error)
  }
}

const listUsers = async (req, res, next) => {
  try {
    let filters = req.body
    let users = await list(filters)
    res.status(200).json({
      success: 1,
      users,
      message: 'Usuarios listados con éxito',
    })
  } catch (error) {
    next(error)
  }
}

const editUser = async (req, res, next) => {
  try {
    let { id } = req.params
    let changes = req.body
    let user = await edit(id, changes)
    res.status(200).json({
      success: 1,
      user,
      message: 'Usuario editado con éxito',
    })
  } catch (error) {
    next(error)
  }
}

const delUser = async (req, res, next) => {
  try {
    let { id } = req.params
    let user = await del(id)
    res.status(200).json({
      success: 1,
      user,
      message: 'Usuario eliminado con éxito',
    })
  } catch (error) {
    next(error)
  }
}

const loginUser = async (req, res, next) => {
  try {
    let { username, password } = req.body
    let user = await login(username, password)
    res.status(200).json({
      success: 1,
      user,
      message: 'Inicio de sesión éxitoso',
    })
  } catch (error) {
    next(error)
  }
}

const checkTokenUser = async (req, res, next) => {
  try {
    let { token, userId } = req.body
    let isAccess = await checkToken(token, userId)
    res.status(200).json({
      success: 1,
      isAccess,
      message: 'Token verificado con éxito',
    })
  } catch (error) {
    next(error)
  }
}

module.exports = {
  createUser,
  getUser,
  listUsers,
  delUser,
  editUser,
  loginUser,
  checkTokenUser,
}
