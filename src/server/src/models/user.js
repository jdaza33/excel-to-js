/**
 * Esquema de usuarios
 */

//Modules
const mongoose = require('mongoose')
const Schema = mongoose.Schema

//Schema
const UserSchema = new Schema({
  name: { type: String, unique: false, required: true },
  lastname: { type: String, unique: false, required: false },
  username: { type: String, unique: true, required: true },
  password: { type: String, unique: false, required: true },
  email: { type: String, unique: true, required: true },
  active: { type: Boolean, default: true },
  createdAt: { type: Number, unique: false, required: true }, //Fecha en milisegundos
  role: { type: [String], unique: false, required: false },
})

module.exports = mongoose.model('Users', UserSchema)
