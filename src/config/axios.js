/**
 * @description Configuracion global de axios
 */

import axios from 'axios'
import dotenv from 'dotenv'
dotenv.config()

const PORT = process.env.VUE_APP_API_URL

const instance = axios.create({
  baseURL: PORT,
  timeout: 1500000,
})

export default instance
