export default [
  {
    title: 'Días',
    role: 'dias',
    children: [
      { title: 'Exactos', name: 'ExactosFuncionDias' },
      { title: 'Aproximado', name: 'ExactosFuncionDiasTresSesenta' },
      { title: 'Banqueros', name: 'ExactosBanqueros' },
    ],
  },
  {
    title: 'Interes Simple S',
    role: 'interes_simple_s',
    children: [
      {
        title: 'Valor futuro o intereses',
        name: 'InteresSimpleSUno',
      },
      {
        title: 'Valor presente o intereses',
        name: 'InteresSimpleSDos',
      },
      { title: 'Tasa de interés', name: 'InteresSimpleSTres' },
      {
        title: 'Tiempo',
        name: 'InteresSimpleSCuatro',
      },
    ],
  },
  {
    title: 'I = Cit',
    role: 'interes_simple_i',
    children: [
      {
        title: 'Capital y valor futuro',
        name: 'InteresSimpleIUno',
      },
      {
        title: 'Tasa de interés y valor futuro',
        name: 'InteresSimpleIDos',
      },
      {
        title: 'Tiempo',
        name: 'InteresSimpleITres',
      },
    ],
  },
  {
    title: 'Descuento',
    role: 'descuento',
    children: [
      { title: 'Valor por Pagar', name: 'DescuentoUno' },
      {
        title: 'Cuando se ocupa cantidad neta recibida y descuento',
        name: 'DescuentoDos',
      },
      { title: 'Valor futuro', name: 'DescuentoTres' },
      { title: 'Descuento Único', name: 'DescuentoCuatro' },
    ],
  },
  {
    title: 'Pagos Parciales',
    role: 'pagos_parciales',
    children: [
      { title: 'Regla Comercial', name: 'PagosParciales' },
      {
        title: 'Regla de estados unidos o reglas de los saldos insolutos',
        name: 'PagosParcialesDos',
      },
      { title: 'Regla del lagarto o cocodrilo', name: 'PagosParcialesTres' },
    ],
  },
  {
    title: 'Series Pagos',
    role: 'series_pagos',
    children: [
      {
        title: 'Costo actual o costo hoy (Valor Presente)',
        name: 'SeriesPagosUno',
      },
      { title: 'Costo que tendria o costo total', name: 'SeriesPagosDos' },
    ],
  },
  {
    title: 'Flj. Efec. por Fechas',
    role: 'flj',
    children: [{ title: 'Flujo efectivo por fechas', name: 'FlujoEfectivo' }],
  },
  {
    title: 'Prueba Pendiente',
    role: 'prueba_pendiente',
    children: [{ title: 'Prueba Pendiente', name: 'PruebaPendiente' }],
  },
  {
    title: 'Hipo',
    role: 'hipo',
    children: [
      { title: 'Hipo 1 Muestra Z', name: 'HipoUnoMuestraZ' },
      { title: 'Hipo 1 Muestra T', name: 'HipoUnoMuestraT' },
      { title: 'Hipo 1 Muestra T Resumido', name: 'HipoUnoMuestraTR' },
    ],
  },
  {
    title: 'Dos Medias',
    role: 'dos_medias',
    children: [
      { title: 'Dos Media U con Z', name: 'DosMediaUUno' },
      { title: 'Dos Media U con T', name: 'DosMediaUDos' },
      { title: 'Dos Media U T Resumido', name: 'DosMediaUTres' },
      { title: 'Dos Media U T Diferentes', name: 'DosMediaUCuatro' },
      { title: 'Dos Media U T Diferent Resums', name: 'DosMediaUCinco' },
      { title: 'Dos Media U Dependientes', name: 'DosMediaUSeis' },
    ],
  },
  {
    title: 'Estimación',
    role: 'estimacion',
    children: [
      { title: 'Estimación Desviación', name: 'EstDesUno' },
      { title: 'Estimación Proporción', name: 'EstDesDos' },
      { title: 'Inv Conf Media Z', name: 'EstDesTres' },
      { title: 'Inv Conf Z Media Finita', name: 'EstDesCuatro' },
      { title: 'Inv Conf Z Proporcion', name: 'EstDesCinco' },
      { title: 'Inv Conf Proporcion Finita', name: 'EstDesSeis' },
      { title: 'Inv Conf T Datos', name: 'EstDesSiete' },
      { title: 'Inv Conf T Resumidos', name: 'EstDesOcho' },
      { title: 'Inv Conf T Resumido Finito', name: 'EstDesNueve' },
    ],
  },
  {
    title: 'Probabilidad',
    role: 'probabilidad',
    children: [
      { title: 'Dist. Prob. Discreta', name: 'ProbUno' },
      { title: 'Dist. Prob. Discreta Frecuencia', name: 'ProbDos' },
      { title: 'Dist. Binomial', name: 'ProbTres' },
      { title: 'Dist. Hispergeometrica', name: 'ProbCuatro' },
      { title: 'Dist. Poisson', name: 'ProbCinco' },
    ],
  },
  {
    title: 'Dist. Continua',
    role: 'dist_continua',
    children: [
      { title: 'Probabilidad Z', name: 'DistContUno' },
      { title: 'Valor X', name: 'DistContDos' },
      { title: 'Teorema Limite Central', name: 'DistContTres' },
    ],
  },
  {
    title: 'Form. Interes Comp.',
    role: 'form_int_comp',
    children: [
      { title: 'Valor presente a futuro', name: 'FormInterComp1' },
      { title: 'Valor futuro a presente', name: 'FormInterComp2' },
      { title: 'Tasa de Interés', name: 'FormInterComp3' },
      { title: 'Periodo', name: 'FormInterComp4' },
      { title: 'Tasa nominales equivalentes', name: 'FormInterComp5' },
      { title: 'Tasas efectivas', name: 'FormInterComp6' },
      { title: 'Numero de periodos', name: 'FormInterComp7' },
      { title: 'Interés comp. presente', name: 'FormInterComp8' },
      { title: 'Interés comp. futuro', name: 'FormInterComp9' },
    ],
  },
  {
    title: 'Tasa Mixta',
    role: 'tasa_mixta',
    children: [
      { title: 'Valor Presente', name: 'TasaMixta1' },
      { title: 'Valor Futuro', name: 'TasaMixta2' },
    ],
  },
  {
    title: 'Ingreso Mixto',
    role: 'ingreso_mixto',
    children: [
      { title: 'Valor Presente Mixto', name: 'IngresoMixto1' },
      { title: 'Valor Futuro Mixto', name: 'IngresoMixto2' },
    ],
  },
]
