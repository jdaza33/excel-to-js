import Vue from 'vue'
import App from './App.vue'
import router from './router'
import Buefy from 'buefy'
import 'buefy/dist/buefy.css'
import VueBus from 'vue-bus'
import VueCookies from 'vue-cookies'

Vue.use(VueCookies)
Vue.use(VueBus)
Vue.use(Buefy)

Vue.filter('abs', function(value) {
  if (!value) return 0
  return Math.abs(value)
})

Vue.filter('x100', function(value) {
  if (!value) return 0
  return Math.abs(value) * 100
})
Vue.filter('round', function(value) {
  if (!value) return 0
  return Math.round(value * 100) / 100
})
Vue.filter('round3', function(value) {
  if (!value) return 0
  return Math.round(value * 1000) / 1000
})
Vue.filter('round4', function(value) {
  if (!value) return 0
  return Math.round(value * 10000) / 10000
})

Vue.mixin({
  methods: {
    reset: function() {
      Object.assign(this.$data, this.$options.data())
    },
  },
})

Vue.mixin({
  methods: {
    resetSelected: function(obj, op) {
      for (let o of obj) {
        this[o] = 0
      }
      if (op == 1) this.blockCalcular1 = false
      if (op == 2) this.blockCalcular2 = false
      if (op == 3) this.blockCalcular3 = false
      if (op == 4) this.blockCalcular4 = false
      if (op == 5) this.blockCalcular5 = false
    },
  },
})

Vue.mixin({
  data: function() {
    return {
      get blockCalcular() {
        return false
      },
    }
  },
})
Vue.mixin({
  data: function() {
    return {
      get blockCalcular1() {
        return false
      },
    }
  },
})
Vue.mixin({
  data: function() {
    return {
      get blockCalcular2() {
        return false
      },
    }
  },
})
Vue.mixin({
  data: function() {
    return {
      get blockCalcular3() {
        return false
      },
    }
  },
})
Vue.mixin({
  data: function() {
    return {
      get blockCalcular4() {
        return false
      },
    }
  },
})
Vue.mixin({
  data: function() {
    return {
      get blockCalcular5() {
        return false
      },
    }
  },
})

Vue.config.productionTip = false

new Vue({
  router,
  render: (h) => h(App),
}).$mount('#app')
