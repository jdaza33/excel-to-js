'use strict'

function Days360(sd, fd, m) {
  var d1 = new Date(sd)
  var d2 = new Date(fd)
  var d1_1 = d1
  var d2_1 = d2
  var method = m || false
  var d1_y = d1.getFullYear()
  var d2_y = d2.getFullYear()
  var dy = 0
  var d1_m = d1.getMonth()
  var d2_m = d2.getMonth()
  var dm = 0
  var d1_d = d1.getDate()
  var d2_d = d2.getDate()
  var dd = 0
  if (method) {
    // euro
    if (d1_d == 31) d1_d = 30
    if (d2_d == 31) d2_d = 30
  } else {
    // american NASD
    if (d1_d == 31) d1_d = 30
    if (d2_d == 31) {
      if (d1_d < 30) {
        if (d2_m == 11) {
          d2_y = d2_y + 1
          d2_m = 0
          d2_d = 1
        } else {
          d2_m = d2_m + 1
          d2_d = 1
        }
      } else {
        d2_d = 30
      }
    }
  }
  dy = d2_y - d1_y
  dm = d2_m - d1_m
  dd = d2_d - d1_d
  return parseFloat(dy * 360 + dm * 30 + dd)
}

module.exports = { Days360 }
