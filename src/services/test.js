const jStat = require('jstat')
const jsstats = require('js-stats')
const formula = require('@formulajs/formulajs')

const calcularInv = (a, b) => {
  // let res = jStat.beta.inv(2, 0.75, 178)
  var normal_distribution = new jsstats.NormalDistribution(0, 1)
  // return normal_distribution.cumulativeProbability(2.5);
  // return t_distribution.invCumulativeProbability(0.05) //.toFixed(3)

  // let cs_distribution = new jsstats.FDistribution(0.05, 4)
  // return cs_distribution.cumulativeProbability(0.05)

  // var mu = 0.0 // mean
  // var sd = 1.0 // standard deviation
  // var normal_distribution = new jsstats.NormalDistribution(0, 1)
  // return normal_distribution.invCumulativeProbability(0.908789)

  // return 1 - jStat.normal.inv(0.03, 2100, 250) //x N n S

  return jStat.stdev([2, 4, 5, 10, 2], true)
  // return formula.TINV(0.99,4)
}

console.log(calcularInv())
