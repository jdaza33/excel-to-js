/**
 * @description Servicio de usuarios
 */

//Modules
import axios from '../config/axios'
import Cookies from 'js-cookie'

//Constantes
const userCookies = [
  'name',
  'lastname',
  'role',
  'email',
  '_id',
  'token',
  'username',
]

//Utils
const setCookies = (data = {}) => {
  for (let i in data) {
    if (userCookies.includes(i))
      Cookies.set(`${i == '_id' ? 'userId' : i}`, data[i])
  }
}
const removeAllCookies = () => {
  for (let i of userCookies) {
    Cookies.remove(i == '_id' ? 'userId' : i)
  }
}
const getUserIdCookie = () => {
  return Cookies.get('userId') == '' || Cookies.get('userId') == undefined
    ? 'invited'
    : Cookies.get('userId')
}

/**ENDPOINTS */
const login = async (username, password) => {
  try {
    let { data } = await axios.post(`/api/users/login`, { username, password })
    return {
      success: data.success,
      message: data.message || data.error,
      user: data.user,
    }
  } catch (err) {
    console.log(err)
    return {
      success: 0,
      error: err,
      message: err.response.data.message || err.response.data.error,
    }
  }
}

const checkToken = async (token, userId) => {
  try {
    let { data } = await axios.post(`/api/users/check-token`, { token, userId })
    return {
      success: data.success,
      message: data.message || data.error,
      isAccess: data.isAccess,
    }
  } catch (err) {
    console.log(err)
    return {
      success: 0,
      error: err,
      message: err.response.data.message || err.response.data.error,
    }
  }
}

const listUsers = async () => {
  try {
    let { data } = await axios.post(`/api/users/list`, {})
    return {
      success: data.success,
      message: data.message || data.error,
      users: data.users,
    }
  } catch (err) {
    console.log(err)
    return {
      success: 0,
      error: err,
      message: err.response.data.message || err.response.data.error,
    }
  }
}

const createUser = async (user = {}) => {
  try {
    let { data } = await axios.post(`/api/users/create`, user)
    return {
      success: data.success,
      message: data.message || data.error,
      user: data.user,
    }
  } catch (err) {
    console.log(err)
    return {
      success: 0,
      error: err,
      message: err.response.data.message || err.response.data.error,
    }
  }
}

const delUser = async (id) => {
  try {
    let { data } = await axios.delete(`/api/users/${id}`)
    return {
      success: data.success,
      message: data.message || data.error,
      user: data.user,
    }
  } catch (err) {
    console.log(err)
    return {
      success: 0,
      error: err,
      message: err.response.data.message || err.response.data.error,
    }
  }
}

const getUser = async (id) => {
  try {
    let { data } = await axios.get(`/api/users/${id}`)
    return {
      success: data.success,
      message: data.message || data.error,
      user: data.user,
    }
  } catch (err) {
    console.log(err)
    return {
      success: 0,
      error: err,
      message: err.response.data.message || err.response.data.error,
    }
  }
}

const editUser = async (id, changes) => {
  try {
    let { data } = await axios.put(`/api/users/${id}`, changes)
    return {
      success: data.success,
      message: data.message || data.error,
      user: data.user,
    }
  } catch (err) {
    console.log(err)
    return {
      success: 0,
      error: err,
      message: err.response.data.message || err.response.data.error,
    }
  }
}

export {
  login,
  setCookies,
  removeAllCookies,
  getUserIdCookie,
  checkToken,
  listUsers,
  createUser,
  delUser,
  editUser,
  getUser,
}
